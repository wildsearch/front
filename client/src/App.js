import {SearchBox} from './components/SearchBox/SearchBox';

import background from './images/background.png';
import './App.scss';

function App() {
  return (
    <div className="container shadowBlock" style={{backgroundImage: `url(${background})`}}>
      <div className="search-box">
        <SearchBox />
      </div>
    </div>
  );
}

export default App;
