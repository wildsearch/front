import React, {useCallback, useState} from 'react';
import { Recommendations } from '../Recommendations/Recommendations';
import './SearchBox.scss';

export const SearchBox = () => {
  const [text, setText] = useState('');
  const handleChange = useCallback((e) => {
    setText(e.target.value);
  }, [setText]);
  console.log(text);

  return (
    <div className="search-box-container">
      <input
        className="search-input"
        type="text"
        placeholder="Я ищу..."
        value={text}
        onChange={handleChange}
      />
      <Recommendations text={text} />
    </div>
  );
}