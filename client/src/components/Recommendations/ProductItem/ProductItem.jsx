import {FaArrowRight, FaSearch} from 'react-icons/fa';
import './ProductItem.scss';

export const ProductItem = ({
  name,
  price,
  photo
}) => {
  return (
    <div className="product-item">
      <FaSearch color="#ddd"/>
      <div className="name-price">
        <div className="name">{name}</div>
        {price && <div className="price">{price} &#8381;</div>}
      </div>
      <img src={photo} />
      <div className="align-v-center">
        <FaArrowRight color="#ddd"/>
      </div>
    </div>
  );
}
