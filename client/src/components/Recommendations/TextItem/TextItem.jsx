import Highlighter from "react-highlight-words";
import './TextItem.scss';

export const TextItem = ({
  text,
  fadeText,
}) => {
  return (
    <div className="text-item">
      <Highlighter
        highlightClassName="text-found"
        textToHighlight={text}
        searchWords={[fadeText]}
      />
    </div>
  );
}
