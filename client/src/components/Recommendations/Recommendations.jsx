import {useCallback, useEffect, useState} from 'react';
import axios from 'axios';
import {map, throttle} from 'lodash';

import ProductItem from './ProductItem';
import TextItem from './TextItem';
import './Recommendations.scss';

const url = process.env.REACT_APP_SCRIPT_ADDRESS ?? 'http://51.250.9.222:8000/index.php';

export const Recommendations = ({
  throttleDelay = 300,
  text
}) => {
  const [recommendations, setRecommendations] = useState({});

  const fetchRecommendations = useCallback(throttle((text) => {
    let searchParams = new URLSearchParams({
      r: 'site/search',
      search: text
    });
    searchParams.toString();

    axios
      .get(`${url}?${searchParams}`)
      .then(res => {
        if (res?.status !== 200) {
          return;
        }
        setRecommendations(res.data);
        console.log(res.data);
      })
      .catch(err => {});
  }, throttleDelay), [setRecommendations]);

  useEffect(() => {
    fetchRecommendations(text);
  }, [text]);

  console.log('recommendations', recommendations);

  return <div className="recommendations">
    {!text && map(recommendations.history, ({id, title}) => (
      <TextItem key={id} text={title} />
    ))}
    {map(recommendations.data, ({value}) => (
      <TextItem key={value} text={value} fadeText={text} />
    ))}
    {text && map(recommendations.categories, ({id, title}) => (
      <TextItem key={id} text={title} />
    ))}
    {text && map(recommendations.goods, ({id, title, photo}) => (
      <ProductItem
        key={id}
        name={title}
        photo={photo}
      />
    ))}
  </div>;
}
