<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron bg-transparent">

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <style>
            .twitter-typeahead{display: block!important;}
            .tt-menu.tt-open{width: 100%;}
            .tt-dataset.tt-dataset-best-pictures{ padding: 10px 0; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);}
            .tt-suggestion.tt-selectable{padding: 2px 15px;}
        </style>

        <div id="remote" class="w-100">
            <div class="row">
                <div class="col-12">
                    <form action="">
                        <div class="form-group">
                            <input class="form-control typeahead" type="text" placeholder="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<?php $this->registerJs(<<<JS

    var bestPictures = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        // prefetch: '../data/films/post_1960.json',
        remote: {
            url: "http://localhost:8080/index.php?r=site/search&search=%QUERY",
            wildcard: '%QUERY'
        }
    });

    $('#remote .typeahead').typeahead(null, {
        name: 'best-pictures',
        display: 'value',
        source: bestPictures
    });
  
JS ); ?>