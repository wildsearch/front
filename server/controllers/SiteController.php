<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Json;
use yii\httpclient\Client;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['http://51.250.9.222:3000', 'http://localhost:3000'],
                    'Access-Control-Allow-Methods' => ['POST'],
                    'Access-Control-Allow-Headers' => ['Content-Type, Authorization'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSearch($search)
    {
        // $config = ['host'=>'51.250.9.222','port'=>9308];
        // $client = new \Manticoresearch\Client($config);
        // $index = $client->index('popularity');

        // $results = $index->search($search.'*')->get();
        $results = $this->getApiQuery($search);

        $data = [];
        foreach($results as $key => $value) {
            $data[] = [
                "key" => $key,
                "value" => $value
            ];
        }
        $categories = [];
        for ($i=0; $i < 10; $i++) { 
            $categories[] = [
                "id" => rand(100000,999999),
                "title" => 'Категория ' . rand(1,20)
            ];
        }
        $history = [];
        for ($i=0; $i < 10; $i++) { 
            $history[] = [
                "id" => rand(100000,999999),
                "title" => 'История ' . rand(1,20),
                "ranking" => $i
            ];
        }
        $goods = [];
        for ($i=0; $i < 10; $i++) { 
            $id = rand(1,20);
            $goods[] = [
                "id" => rand(100000,999999),
                "title" => 'Товар ' . $id,
                "photo" => "https://picsum.photos/id/$id/200/300",
            ];
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['data' => $data, 'categories' => $categories, 'history' => $history, 'goods' => $goods];
    }

    protected function getApiQuery($text)
    {
        $headers = ['Content-Type' => 'application/json; charset=utf-8'];
        $client = new \GuzzleHttp\Client(['headers' => $headers, 'base_uri' => 'http://51.250.9.222:5000/recommend']);
        $response = $client->request('POST', '/recommend', ['json' => ['query' => $text]]);
        return json_decode($response->getBody()->getContents())->autocomplete;
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
